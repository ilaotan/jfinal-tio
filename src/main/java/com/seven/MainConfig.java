package com.seven;

import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.template.Engine;
import com.seven.client.HelloClientStarter;
import com.seven.controller.HelloTioController;
import com.seven.server.HelloServerStarter;


public class MainConfig extends JFinalConfig {

    private static Prop p = loadConfig();

    private static Prop loadConfig() {
        return PropKit.use("dev_config.txt").appendIfExists("roduct_config.txt");
    }

    @Override
    public void configConstant(Constants me) {
        me.setDevMode(p.getBoolean("devMode"));
    }

    @Override
    public void configRoute(Routes me) {
        me.add("/", HelloTioController.class);
    }

    @Override
    public void configPlugin(Plugins me) {
        me.add(new HelloServerStarter());
        me.add(new HelloClientStarter());
    }

    @Override
    public void configInterceptor(Interceptors me) {

    }

    @Override
    public void configHandler(Handlers me) {

    }

    @Override
    public void configEngine(Engine me) {

    }

    @Override
    public void onStart() {

    }

    public static void main(String[] args) {
        //eclipse启动方式
        //JFinal.start("src/main/webapp", 80, "/");

        //idea
        JFinal.start("src/main/webapp", 80, "/",5);

    }

}
